import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { AccidentCardComponent } from './accident/accident-card/accident-card.component';
import { AccidentFeedComponent } from './accident/accident-feed/accident-feed.component';
import { AccidentDetailComponent } from './accident/accident-detail/accident-detail.component';
import { AccidentCreateComponent } from './accident/accident-create/accident-create.component';

import { AccidentService } from './accident/accident.service';

@NgModule({
  declarations: [
    AppComponent,
    AccidentCardComponent,
    AccidentFeedComponent,
    AccidentDetailComponent,
    AccidentCreateComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    AccidentService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
