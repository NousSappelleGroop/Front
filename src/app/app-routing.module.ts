import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
 
import { AccidentFeedComponent } from './accident/accident-feed/accident-feed.component';
import { AccidentDetailComponent } from './accident/accident-detail/accident-detail.component';
 import { AccidentCreateComponent } from './accident/accident-create/accident-create.component';

const routes: Routes = [
  { path: '', redirectTo: '/feed', pathMatch: 'full' },
  { path: 'feed', component: AccidentFeedComponent },
  { path: 'report', component: AccidentCreateComponent },
  { path: 'accident/:accident_id', component: AccidentDetailComponent },
];
 
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}