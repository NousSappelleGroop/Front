export class Accident {
	_id: string;
	createdAt: Date;
	updatedAt: Date;
	author: string;
	nbVehicles: number;
	date : Date;
	localisation : string;

	constructor(author?: string, nbVehicules?: number, date?: Date, localisation?: string)
	{
		this.author = author;
		this.nbVehicles = nbVehicules;
		this.date = date;
		this.localisation = localisation;
	}
}
