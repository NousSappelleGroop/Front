import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute  } from '@angular/router';
import { Accident } from '../accident.class';
import { AccidentService } from '../accident.service';

@Component({
  selector: 'app-accident-detail',
  templateUrl: './accident-detail.component.html',
  styleUrls: ['./accident-detail.component.css']
})
export class AccidentDetailComponent implements OnInit {

  public accident: Accident;

  constructor(private accidentService: AccidentService, private route: ActivatedRoute, private router: Router) {  	
  }

  ngOnInit() {
    this.loadAccident()
  }

  loadAccident(){
    const accidentId = this.route.snapshot.paramMap.get('accident_id');
    this.accidentService.getAccident(accidentId)
    .subscribe((accident) => this.accident = accident );
  }

}
