import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Accident } from '../accident.class';
import { AccidentService } from '../accident.service';

@Component({
  selector: 'app-accident-create',
  templateUrl: './accident-create.component.html',
  styleUrls: ['./accident-create.component.css']
})
export class AccidentCreateComponent implements OnInit {

  public accident: Accident;

  constructor(private accidentService: AccidentService, private router: Router) {  	
  }

  ngOnInit() {
    this.accident = new Accident();
  }

  addAccident(){
    this.accidentService.addAccident(this.accident)
    .subscribe(() => this.router.navigate(['/feed']));
  }

}
