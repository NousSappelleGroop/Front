import { Component, OnInit, Input } from '@angular/core';
import { Accident } from '../accident.class';

@Component({
  selector: 'app-accident-card',
  templateUrl: './accident-card.component.html',
  styleUrls: ['./accident-card.component.css']
})
export class AccidentCardComponent implements OnInit {
	@Input() accident : Accident;

  constructor() { }

  ngOnInit() {
  }

}
