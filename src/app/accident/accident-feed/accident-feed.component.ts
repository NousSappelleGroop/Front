import { Component, OnInit } from '@angular/core';
import { Accident } from '../accident.class';
import { AccidentService } from '../accident.service';

@Component({
  selector: 'app-accident-feed',
  templateUrl: './accident-feed.component.html',
  styleUrls: ['./accident-feed.component.css']
})
export class AccidentFeedComponent implements OnInit {

	public accidents = [];

  constructor(public accidentService: AccidentService) {
  }

  ngOnInit() {
    this.loadAccidentts();
  }

  loadAccidentts(){
    this.accidentService.getAccidents()
    .subscribe(heroes => this.accidents = heroes);
  }

}
