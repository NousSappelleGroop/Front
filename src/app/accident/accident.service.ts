import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
 
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
 
import { Accident } from './accident.class';
 
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
 
@Injectable()
export class AccidentService {
 
  private accidentsUrl = 'api/accidents';
 
  constructor(private http: HttpClient) { }
 
  getAccidents (): Observable<Accident[]> {
    return this.http.get<Accident[]>(this.accidentsUrl)
      .pipe(
        tap(heroes => {}),
        catchError(this.handleError('getHeroes', []))
      );
  }

  getAccident(_id: string): Observable<Accident> {
    const url = `${this.accidentsUrl}/${_id}`;
    return this.http.get<Accident>(url).pipe(
      tap(_ => console.log(`fetched hero id=${_id}`)),
      catchError(this.handleError<Accident>(`getAccident id=${_id}`))
    );
  }
   
  addAccident (accident: Accident): Observable<Accident> {
    return this.http.post<Accident>(this.accidentsUrl, accident, httpOptions).pipe(
      tap((accident: Accident) => console.log(`added hero w/ id=${accident._id}`)),
      catchError(this.handleError<Accident>('addHero'))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}